import React from "react";
import { useNavigate } from "react-router-dom";

const Orders = () => {
  const navigate = useNavigate();
  return (
    <div className="maincont">
      <div className="topoftable">
        <div className="leftsidetable">Orders</div>
        <div className="tabletopright">
          {/* <button
            type="button"
            className="btn btn-success"
            onClick={() => navigate("/admin/food/addfood")}
          >
            Add Order
          </button> */}
          {/* <button
            type="button"
            className="btn btn-light"
            onClick={() => navigate("/admin/food/addfood")}
          >
            Edit Order
          </button> */}
        </div>
      </div>
      <div className="mainforfood">
        <table className="table table-striped">
          <thead>
            <tr>
              <th scope="col">#</th>
              <th scope="col">First</th>
              <th scope="col">Last</th>
              <th scope="col">Handle</th>
              <th scope="col" colSpan={2}>
                Actions
              </th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <th scope="row">1</th>
              <td>Mark</td>
              <td>Otto</td>
              <td>@mdo</td>
              <td>Edit</td>
              <td>delete</td>
            </tr>
            <tr>
              <th scope="row">2</th>
              <td>Jacob</td>
              <td>Thornton</td>
              <td>@fat</td>
              <td>Edit</td>
              <td>delete</td>
            </tr>
            <tr>
              <th scope="row">3</th>
              <td>Larry</td>
              <td>the Bird</td>
              <td>@twitter</td>
              <td>Edit</td>
              <td>delete</td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  );
};

export default Orders;
