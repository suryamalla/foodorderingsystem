import "./App.css";
import "../node_modules/bootstrap-dark-5/dist/css/bootstrap-dark.min.css"; //npm i bootstrap-dark-5 boostrap
import "../node_modules/bootstrap/dist/js/bootstrap.bundle";
import "../node_modules/bootstrap/dist/js/bootstrap.bundle.min.js";

import Home from "./screens/Home";
import "./style/style.css";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
// import Navbar from './components/Navbar';
import Login from "./screens/Login";
import Signup from "./screens/Signup";
import { CartProvider } from "./components/ContextReducer";
import MyOrder from "./screens/MyOrder";
import AdminLayout from "./layout/adminLayout";
import Dashboard from "./container/admin";
import ClientLayout from "./layout/clientLayout";
import PageNotFound from "./screens/pagenotfound";
import Food from "./container/admin/food";
import Category from "./container/admin/category";
import Profile from "./container/admin/profile";
import Client from "./container/admin/clients";
import Admins from "./container/admin/admins";
import FoodForm from "./components/FoodForm";
import Orders from "./container/admin/orders";
function App() {
  return (
    <CartProvider>
      <Router>
        <div>
          <Routes>
            <Route exact path="/" element={<Home />} />
            <Route exact path="/login" element={<Login />} />
            <Route exact path="/signup" element={<Signup />} />
            <Route element={<ClientLayout />}>
              <Route exact path="/myorder" element={<MyOrder />} />
            </Route>

            <Route element={<AdminLayout />}>
              <Route path="/admin" element={<Dashboard />} />
              <Route path="/admin/food" element={<Food />} />
              <Route path="/admin/food/:id" element={<FoodForm />} />
              <Route path="/admin/category" element={<Category />} />
              <Route path="/admin/profile" element={<Profile />} />
              <Route path="/admin/clients" element={<Client />} />
              <Route path="/admin/admins" element={<Admins />} />
              <Route path="/admin/orders" element={<Orders />} />
            </Route>
            <Route path="*" element={<PageNotFound />} />
          </Routes>
        </div>
      </Router>
    </CartProvider>
  );
}

export default App;
