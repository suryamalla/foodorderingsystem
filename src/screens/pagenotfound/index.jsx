import React from "react";

const PageNotFound = () => {
  return (
    <>
      <div className="h-2"></div>
      <div className="h-72"></div>
      <div className="h-96">
        {" "}
        <center>404 Page Not Found</center>
      </div>
    </>
  );
};

export default PageNotFound;
