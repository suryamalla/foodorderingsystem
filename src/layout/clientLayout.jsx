import React, { useEffect, useState } from "react";
import { Navigate, Outlet, useLocation } from "react-router-dom";

const ClientLayout = () => {
  const location = useLocation();
  //   const [token, setToken] = useState(false);
  let isToken = localStorage.getItem("token");
  //   useEffect(() => {
  //     localStorage.getItem("token") ? setToken(true) : setToken(false);
  //   }, []);
  return (
    <>
      {isToken !== null && isToken ? (
        <Outlet />
      ) : (
        <Navigate to="/login" state={{ form: location }} replace />
      )}
    </>
  );
};

export default ClientLayout;
