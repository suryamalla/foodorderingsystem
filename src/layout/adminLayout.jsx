import React, { useEffect } from "react";
import { Outlet } from "react-router-dom";
import HoriNav from "../components/horinav";
import SideNav from "../components/sidenav";
import useWindowDimensions from "../components/windowresize";
const AdminLayout = () => {
  const { width } = useWindowDimensions();
  return (
    <>
      {width > 450 ? (
        <div className="flextonavandchildren">
          <SideNav />
          <Outlet />
        </div>
      ) : (
        <div className="flexcol">
          <HoriNav />
          <div className="paddings">
            <Outlet />
          </div>
        </div>
      )}
    </>
  );
};

export default AdminLayout;
