import {
  CDBSidebar,
  CDBSidebarContent,
  CDBSidebarHeader,
  CDBSidebarMenu,
  CDBSidebarMenuItem,
} from "cdbreact";
import { Link, NavLink } from "react-router-dom";
const Sidebar = () => {
  return (
    <>
      <div
        style={{ display: "flex", height: "100%", overflow: "scroll initial" }}
      >
        <CDBSidebar
          // textColer="#fff"
          backgroundColor="green"
          className="headerside"
        >
          <CDBSidebarHeader prefix={<i className="fa fa-bars fa-large "></i>}>
            <Link to="/admin" className="textcolor">
              <br />
              Dashboard
            </Link>
          </CDBSidebarHeader>
          <CDBSidebarContent className="sidebar-content ">
            <CDBSidebarMenu>
              <NavLink to="#" activeClassName="activeClicked">
                <CDBSidebarMenuItem icon="columns">
                  <Link to="/admin/orders" className="textcolor">
                    Orders
                  </Link>
                </CDBSidebarMenuItem>
              </NavLink>
              <NavLink to="#" activeClassName="activeClicked">
                <CDBSidebarMenuItem icon="columns">
                  <Link to="/admin/Category" className="textcolor">
                    Category
                  </Link>
                </CDBSidebarMenuItem>
              </NavLink>
              <NavLink to="#" activeClassName="activeClicked">
                <CDBSidebarMenuItem icon="columns">
                  {" "}
                  <Link to="/admin/Food" className="textcolor">
                    <span className="textcolorlink">Food</span>
                  </Link>
                </CDBSidebarMenuItem>
              </NavLink>
              <NavLink to="#" activeClassName="activeClicked">
                <CDBSidebarMenuItem icon="columns">
                  {" "}
                  <Link to="/admin/clients" className="textcolor">
                    Client
                  </Link>
                </CDBSidebarMenuItem>
              </NavLink>
              <NavLink to="#" activeClassName="activeClicked">
                <CDBSidebarMenuItem icon="columns">
                  {" "}
                  <Link to="/admin/admins" className="textcolor">
                    Admin
                  </Link>
                </CDBSidebarMenuItem>
              </NavLink>
              <NavLink to="#" activeClassName="activeClicked">
                <CDBSidebarMenuItem icon="columns">
                  {" "}
                  <Link to="/admin/profile" className="textcolor">
                    Profile
                  </Link>
                </CDBSidebarMenuItem>
              </NavLink>
            </CDBSidebarMenu>
          </CDBSidebarContent>
        </CDBSidebar>
      </div>
    </>
  );
};

export default Sidebar;
