import React from "react";
import { Navbar, Nav, Container } from "react-bootstrap";
import { Link } from "react-router-dom";
const HoriNav = () => {
  return (
    <Navbar
      collapseOnSelect
      bg="success"
      // textColer="#fff"
      variant="dark"
      expand="sm"
      fixed="top"
      className="smallnavbar"
    >
      <Container>
        <Navbar.Toggle aria-controls="responsive-navbar-nav" />
        <Navbar.Collapse id="responsive-navbar-nav">
          <Nav className="heightvh">
            <Nav.Link href="#" className="textcolorlink">
              <Link to="/admin" className="textcolor">
                <br />
                Dashboard
              </Link>
            </Nav.Link>
            <Nav.Link href="#" className="textcolorlink">
              <Link to="/admin/Category" className="textcolor">
                Category
              </Link>
            </Nav.Link>
            <Nav.Link href="#">
              <Link to="/admin/Food" className="textcolor">
                <span className="textcolorlink">Food</span>
              </Link>
            </Nav.Link>
            <Nav.Link href="#" className="textcolorlink">
              <Link to="/admin/clients" className="textcolor">
                Client
              </Link>
            </Nav.Link>
            <Nav.Link href="#" className="textcolorlink">
              <Link to="/admin/admins" className="textcolor">
                Admin
              </Link>
            </Nav.Link>
            <Nav.Link href="#" className="textcolorlink">
              <Link to="/admin/profile" className="textcolor">
                Profile
              </Link>
            </Nav.Link>
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
};

export default HoriNav;
